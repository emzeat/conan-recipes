# conanfile.py
#
# Copyright (c) 2022 - 2023 Marius Zwicker
# All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from conan import ConanFile
from conan.tools.files import download, get, copy, save
import os
import textwrap

class ClangToolsExtraConan(ConanFile):
    name = "clang-tools-extra"
    description = "Clang tools for formatting and linting."
    url = "https://emzeat.de/conan-recipes/recipes/clang-tools-extra"
    homepage = "https://clang.llvm.org/"
    license = "Apache License 2.0"

    settings = "os", "arch"
    package_type = "application"

    @property
    def _os_build(self):
        return f"{self.settings.os}_{self.settings.arch}"

    @property
    def _binary_data(self):
        return self.conan_data["binaries"][self.version][self._os_build]

    def build_requirements(self):
        if self._binary_data['url'].endswith('.exe'):
            self.build_requires("7zip/19.00")

    def build(self):
        # data is platform specific so we need to fetch it
        # as part of build() and not source()
        binary_data = self._binary_data
        if binary_data['url'].endswith('.exe'):
            filename = f'clang-tidy-{self.version}.exe'
            download(self, **binary_data, filename=filename)
            self.run(f'7z x -o. {filename}')
        else:
            get(self, **binary_data, strip_root=True)

    def package_id(self):
        if self.info.settings.os in ['Macos']:
            del self.info.settings.os.version
            del self.info.settings.os.sdk_version

    def package(self):
        if self.settings.os in ['Macos', 'Linux']:
            copy(self, "bin/clang-format", src=self.source_path, dst=self.package_path)
            copy(self, "bin/clang-tidy", src=self.source_path, dst=self.package_path)
            copy(self, "include/**", src=self.source_path, dst=self.package_path)
            copy(self, "lib/**", excludes=['*.dylib','*.so*', '*.a'], src=self.source_path, dst=self.package_path)
            bin_suffix = ''
        elif self.settings.os == 'Windows':
            copy(self, "bin/clang-format.exe", src=self.source_path, dst=self.package_path)
            copy(self, "bin/clang-tidy.exe", src=self.source_path, dst=self.package_path)
            copy(self, "bin/api-ms-win-crt*", src=self.source_path, dst=self.package_path)
            copy(self, "bin/vcruntime*", src=self.source_path, dst=self.package_path)
            copy(self, "bin/msvc*", src=self.source_path, dst=self.package_path)
            copy(self, "bin/ucrt*", src=self.source_path, dst=self.package_path)
            copy(self, "include/**", src=self.source_path, dst=self.package_path)
            copy(self, "lib/**", excludes=['*.dll', '*.lib'], src=self.source_path, dst=self.package_path)
            bin_suffix = '.exe'
        save(self, self.package_path / "lib/cmake/clang-tools-extra-module.cmake", textwrap.dedent(f"""
            if(clang-tools-extra_PACKAGE_FOLDER_RELEASE)
                set(clang-tools-extra_BIN_DIRS ${{clang-tools-extra_PACKAGE_FOLDER_RELEASE}}/bin)
            endif()
            if(clang-tools-extra_PACKAGE_FOLDER_DEBUG)
                set(clang-tools-extra_BIN_DIRS ${{clang-tools-extra_PACKAGE_FOLDER_DEBUG}}/bin)
            endif()

            if(NOT TARGET clang-tools-extra::clang-tidy)
                add_executable(clang-tools-extra::clang-tidy IMPORTED GLOBAL)
            endif()
            set_property(TARGET clang-tools-extra::clang-tidy PROPERTY IMPORTED_LOCATION ${{clang-tools-extra_BIN_DIRS}}/clang-tidy{bin_suffix})

            if(NOT TARGET clang-tools-extra::clang-format)
                add_executable(clang-tools-extra::clang-format IMPORTED GLOBAL)
            endif()
            set_property(TARGET clang-tools-extra::clang-format PROPERTY IMPORTED_LOCATION ${{clang-tools-extra_BIN_DIRS}}/clang-format{bin_suffix})
        """))

    def package_info(self):
        self.cpp_info.set_property("cmake_file_name", "clang-tools-extra")
        self.cpp_info.set_property("cmake_build_modules", ["lib/cmake/clang-tools-extra-module.cmake"])

        self.cpp_info.names["cmake_find_package"] = "clang-tools-extra"
        self.cpp_info.names["cmake_find_package_multi"] = "clang-tools-extra"
        self.cpp_info.libdirs = []

        bin_path = self.package_path / "bin"
        self.output.info(f"Appending PATH environment variable: {bin_path}")
        self.buildenv_info.append_path('PATH', bin_path.as_posix())

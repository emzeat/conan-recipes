# conanfile.py
#
# Copyright (c) 2022 - 2023 Marius Zwicker
# All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from conans import ConanFile, tools, CMake
from conan.tools.cmake import CMakeToolchain, cmake_layout
import os

class QZXingConan(ConanFile):
    name = "qzxing"
    description = "Qt/QML wrapper library for the ZXing library. 1D/2D barcode image processing library."
    url = "https://emzeat.de/conan-recipes/recipes/qzxing"
    homepage = "https://github.com/ftylitak/qzxing"
    license = "Apache License"

    settings = "os", "compiler", "build_type", "arch"

    short_paths = True
    generators = "cmake", "cmake_find_package"

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    @property
    def _build_subfolder(self):
        return "build_subfolder"

    def export_sources(self):
        for patch in self.conan_data.get("patches", {}).get(self.version, []):
            self.copy(patch["patch_file"])

    def source(self):
        tools.get(
            **self.conan_data["sources"][self.version],
            strip_root=True, destination=self._source_subfolder)
        for patch in self.conan_data.get("patches", {}).get(self.version, []):
            print(f"Patch source: {patch}")
            tools.patch(**patch)

    def build_requirements(self):
        if self.settings.os == "Windows" and self.settings.compiler == "Visual Studio":
            self.build_requires("jom/1.1.3")

    def requirements(self):
        self.requires("qt/5.15.4")

    def config_options(self):
        pass

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_MODULE_PATH"] = self.build_folder.replace("\\", "/")
        cmake.definitions["QZXING_MULTIMEDIA"] = True
        cmake.definitions["QZXING_USE_ENCODER"] = True
        cmake.definitions["QZXING_USE_QML"] = True
        cmake.definitions["QZXING_USE_ENCODER"] = True
        cmake.definitions["CMAKE_CXX_STANDARD"] = 11

        cmake.configure(source_folder=os.path.join(self._source_subfolder, "src"),
                        build_folder=self._build_subfolder)
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()
        self.copy("LICENSE", src=self._source_subfolder, dst="licenses")

    def package_info(self):
        self.cpp_info.set_property("cmake_file_name", "QZXing")
        self.cpp_info.set_property("cmake_target_name", "QZXing::QZXing")
        self.cpp_info.libs = ["qzxing"]
        self.cpp_info.requires = ["qt::qtCore", "qt::qtQml", "qt::qtQuick", "qt::qtMultimedia"]
        self.cpp_info.defines = ["QZXING_QML", "ENABLE_ENCODER_QR_CODE", "DISABLE_LIBRARY_FEATURES", "ENABLE_ENCODER_GENERIC"]

        self.cpp_info.names["cmake_find_package"] = "QZXing"
        self.cpp_info.names["cmake_find_package_multi"] = "QZXing"

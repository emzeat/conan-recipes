

from conan import ConanFile

class OpenSSLUniversal(ConanFile):
    python_requires = "universal_binary/0.1.0@emzeat/external"
    python_requires_extend = "universal_binary.UniversalBinaryBase"

    name = "openssl"
    package_type = "library"
    url = "https://github.com/conan-io/conan-center-index"
    homepage = "https://github.com/openssl/openssl"
    license = "OpenSSL"
    topics = ("openssl", "ssl", "tls", "encryption", "security")
    description = "A toolkit for the Transport Layer Security (TLS) and Secure Sockets Layer (SSL) protocols"

    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "no_asm": [True, False],
        "no_threads": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "no_asm": False,
        "no_threads": False,
    }



from conan import ConanFile
from conan.tools.scm import Version
from conan.tools.microsoft import is_msvc

class LibPngUniversal(ConanFile):
    python_requires = "universal_binary/0.1.0@emzeat/external"
    python_requires_extend = "universal_binary.UniversalBinaryBase"

    name = "libpng"
    description = "libpng is the official PNG file format reference library."
    url = "https://github.com/conan-io/conan-center-index"
    homepage = "http://www.libpng.org"
    license = "libpng-2.0"
    topics = ("png", "graphics", "image")
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "neon": [True, "check", False],
        "msa": [True, False],
        "sse": [True, False],
        "vsx": [True, False],
        "api_prefix": ["ANY"],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "neon": True,
        "msa": True,
        "sse": True,
        "vsx": True,
        "api_prefix": "",
    }

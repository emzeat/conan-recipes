

from conan import ConanFile
from conan.tools.files import copy
from pathlib import Path
import textwrap
import subprocess
import json
from collections import defaultdict

class UniversalBinaryRecipe(ConanFile):
    name = "universal_binary"
    description = "Base conanfile merging multiple architectures of a lib into a universal binary"
    package_type = "python-require"
    url = "https://emzeat.de/conan-recipes"
    homepage = "https://emzeat.de/conan-recipes"
    license = "Apache 2.0"

    def build(self):
        raise RuntimeError("Only use this as python_requires, no package can be built")


class UniversalBinaryBase:
    settings = "os", "arch", "compiler", "build_type"
    architectures = "armv8", "x86_64"

    @property
    def upstream_version(self):
        '''Split the +universal suffix (if any)'''
        try:
            version, _ = str(self.version).split('+', maxsplit=1)
            return version
        except ValueError:
            return str(self.version)

    def build(self):
        self.output.info(f"Creating universal binary for {self.name}")

        self.artifacts = []
        for arch in self.architectures:
            install_folder = self.build_path / arch
            install_folder.mkdir(parents=True, exist_ok=True)

            options = [f'{self.name}*:{key}={value}' for key, value in self.options.items()]
            options = '\n'.join(options)

            build_profile = install_folder / 'build.profile'
            build_profile.write_text(textwrap.dedent(f"""
            [settings]
            os={self.settings_build.os}
            os.version={self.settings_build.os.version}
            arch={self.settings_build.arch}
            compiler={self.settings_build.compiler}
            compiler.version={self.settings_build.compiler.version}
            compiler.libcxx={self.settings_build.compiler.libcxx}
            build_type=Release

            [options]
            {options}

            [buildenv]
            CONAN_CMAKE_DISABLE_CHECK_COMPILER=ON
            MACOSX_DEPLOYMENT_TARGET={self.settings.os.version}
            """))

            host_profile = install_folder / 'host.profile'
            host_profile.write_text(textwrap.dedent(f"""
            [settings]
            os={self.settings.os}
            os.version={self.settings.os.version}
            arch={arch}
            compiler={self.settings.compiler}
            compiler.version={self.settings.compiler.version}
            compiler.libcxx={self.settings.compiler.libcxx}
            build_type={self.settings.build_type}

            [options]
            {options}

            [buildenv]
            CONAN_CMAKE_DISABLE_CHECK_COMPILER=ON
            MACOSX_DEPLOYMENT_TARGET={self.settings.os.version}
            """))

            conanfile = install_folder / 'conanfile.py'
            conanfile.write_text(textwrap.dedent(f"""
            from conan import ConanFile
            from pathlib import Path
            import json
            import os

            class Installer(ConanFile):
                requires = '{self.name}/{self.upstream_version}'

                def generate(self):
                    for dep_name, dep_cpp_info in self.deps_cpp_info.dependencies:
                        dep = dep_cpp_info
                        break
                    def map_attributes(node):
                        def map_path(v):
                            root_path = str(dep.root_path)
                            return os.path.relpath(v, root_path) if root_path == os.path.commonprefix([v, root_path]) else v
                        mapped = dict()
                        # map basic attributes from cpp_info into a dict making any paths relative
                        for attr in ['includedirs', 'libdirs', 'bindirs', 'libs', 'defines', 'includedirs',
                                'cflags', 'cxxflags', 'cppflags', 'sharedlinkflags', 'exelinkflags', 'frameworks',
                                'frameworkdirs', 'name', 'system_libs', 'build_modules', 'requires', 'objects',
                                'sysroot', 'names', 'filenames']:
                            try:
                                value = getattr(node, attr)
                                if isinstance(value, str):
                                    value = map_path(value)
                                elif isinstance(value, list):
                                    value = [map_path(v) for v in value]
                                mapped[attr] = value
                            except AttributeError:
                                pass
                        # map properties into a dict making any paths relative
                        properties = dict()
                        for prop in ['cmake_file_name', 'cmake_build_modules', 'pkg_config_name',
                                'cmake_find_mode', 'cmake_find_package_multi', 'cmake_find_package', 'cmake_target_name']:
                            value = node.get_property(prop)
                            if value:
                                if isinstance(value, str):
                                    value = map_path(value)
                                elif isinstance(value, list):
                                    value = [map_path(v) for v in value]
                                properties[prop] = value
                        mapped['properties'] = properties
                        return mapped
                    node = {{
                        'rootpath': self.dependencies['{self.name}'].package_folder,
                        'cpp_info': map_attributes(dep),
                        'libdirs': dep.lib_paths,
                        'includedirs': dep.include_paths,
                        'bindirs': dep.bin_paths,
                        'cmake_modules': dep.get_property('cmake_build_modules')
                    }}
                    components = dict()
                    for key, component in dep.components.items():
                        if key is None:
                            continue
                        components[key] = map_attributes(component)
                        node['libdirs'] += component.lib_paths
                        node['includedirs'] += component.include_paths
                        node['bindirs'] += component.bin_paths
                    node['components'] = components
                    node['libdirs'] = list(set(node['libdirs']))
                    node['includedirs'] = list(set(node['includedirs']))
                    node['bindirs'] = list(set(node['bindirs']))
                    if node['cmake_modules']:
                        node['cmake_modules'] = list(set(node['cmake_modules']))
                    else:
                        del node['cmake_modules']
                    if components:
                        # self.cpp_info.components cannot be used with self.cpp_info global values at the same time
                        for entry in ['libs', 'defines', 'requires']:
                            del node['cpp_info'][entry]
                    out = Path('{install_folder}') / 'package_info.json'
                    out.write_text(json.dumps(node, indent=4))
            """))

            subprocess.check_call(['conan', 'install',
                        '-if', install_folder.as_posix(), '-of', install_folder.as_posix(),
                        '-pr:b', build_profile.as_posix(), '-pr:h', host_profile.as_posix(),
                        '-b', 'missing', conanfile.as_posix()])

            package_info = install_folder / 'package_info.json'
            package_info = json.loads(package_info.read_text())
            self.artifacts.append(package_info)

    def _find_lib(self, dir, name):
        '''Discovers a library with name below dir'''
        if not isinstance(dir, Path):
            dir = Path(dir)
        for lib in dir.glob(f'lib{name}.*'):
            return lib
        raise FileNotFoundError(name)

    @property
    def package_info_json(self):
        return self.package_path / 'universal_package_info.json'

    def package(self):
        include_path = self.package_path / 'include'
        include_path.mkdir(exist_ok=True, parents=True)

        lib_path = self.package_path / 'lib'
        lib_path.mkdir(exist_ok=True, parents=True)

        bin_path = self.package_path / 'bin'
        bin_path.mkdir(exist_ok=True, parents=True)

        libs = defaultdict(list)
        bins = defaultdict(list)
        for artifact in self.artifacts:
            for includedir in artifact.get('includedirs', []):
                copy(self, pattern='*', src=includedir, dst=include_path)
            for cmake_module in artifact.get('cmake_modules', []):
                dst = self.package_path / cmake_module
                cmake_module = Path(artifact['rootpath']) / cmake_module
                dst.parent.mkdir(exist_ok=True, parents=True)
                dst.write_text(cmake_module.read_text())
            for libdir in artifact.get('libdirs', []):
                for lib in Path(libdir).glob('*'):
                    if lib.is_file():
                        libs[lib.name].append(lib)
            for bindir in artifact.get('libdirs', []):
                for bin in Path(bindir).glob('*'):
                    if bin.is_file():
                        bins[bin.name].append(bin)

        for lib, inputs in libs.items():
            output = lib_path / lib
            output = output.as_posix()
            inputs = [input.as_posix() for input in inputs]
            self.output.info(f"Merging {inputs} to {output}")
            subprocess.check_call(['lipo'] + inputs + ['-create', '-output', output])

        package_info = {
            'cpp_info': self.artifacts[0]['cpp_info'],
            'components': self.artifacts[0]['components']
        }
        self.package_info_json.write_text(json.dumps(package_info, indent=4))

    def _populate_info(self, cpp_info, property_dict):
        for key, value in property_dict.items():
            if key == 'properties':
                for prop_name, prop_val in value.items():
                    cpp_info.set_property(prop_name, prop_val)
            elif value:
                setattr(cpp_info, key, value)

    def package_info(self):
        package_info = json.loads(self.package_info_json.read_text())

        self._populate_info(self.cpp_info, package_info['cpp_info'])
        for name, component in package_info['components'].items():
            self._populate_info(self.cpp_info.components[name], component)



from conan import ConanFile
from conan.tools.microsoft import is_msvc

class JpegTurboUniversal(ConanFile):
    python_requires = "universal_binary/0.1.0@emzeat/external"
    python_requires_extend = "universal_binary.UniversalBinaryBase"

    name = "libjpeg-turbo"
    description = "SIMD-accelerated libjpeg-compatible JPEG codec library"
    license = ("IJG", "BSD-3-Clause", "Zlib")
    url = "https://github.com/conan-io/conan-center-index"
    homepage = "https://libjpeg-turbo.org"
    topics = ("jpeg", "libjpeg", "image", "multimedia", "format", "graphics")
    provides = "libjpeg"

    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "SIMD": [True, False],
        "arithmetic_encoder": [True, False],
        "arithmetic_decoder": [True, False],
        "libjpeg7_compatibility": [True, False],
        "libjpeg8_compatibility": [True, False],
        "turbojpeg": [True, False],
        "java": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "SIMD": True,
        "arithmetic_encoder": True,
        "arithmetic_decoder": True,
        "libjpeg7_compatibility": True,
        "libjpeg8_compatibility": True,
        "turbojpeg": True,
        "java": False,
    }

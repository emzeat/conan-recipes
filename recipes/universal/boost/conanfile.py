
from conan.errors import ConanException
from conan.tools.scm import Version
from conan.tools.apple import is_apple_os
from conan.tools.microsoft import is_msvc, is_msvc_static_runtime, msvc_runtime_flag, MSBuildToolchain
from conan.tools.files import collect_libs
from conan import ConanFile

import os
import re

class BoostUniversal(ConanFile):
    python_requires = "universal_binary/0.1.0@emzeat/external"
    python_requires_extend = "universal_binary.UniversalBinaryBase"

    name = "boost"
    description = "Boost provides free peer-reviewed portable C++ source libraries"
    url = "https://github.com/conan-io/conan-center-index"
    homepage = "https://www.boost.org"
    license = "BSL-1.0"
    topics = ("libraries", "cpp")

    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "header_only": [True, False],
        "error_code_header_only": [True, False],
        "system_no_deprecated": [True, False],
        "asio_no_deprecated": [True, False],
        "filesystem_no_deprecated": [True, False],
        "filesystem_use_std_fs": [True, False],
        "filesystem_version": [None, "3", "4"],
        "layout": ["system", "versioned", "tagged", "b2-default"],
        "magic_autolink": [True, False],  # enables BOOST_ALL_NO_LIB
        "diagnostic_definitions": [True, False],  # enables BOOST_LIB_DIAGNOSTIC
        "python_executable": [None, "ANY"],  # system default python installation is used, if None
        "python_version": [None, "ANY"],  # major.minor; computed automatically, if None
        "namespace": ["ANY"],  # custom boost namespace for bcp, e.g. myboost
        "namespace_alias": [True, False],  # enable namespace alias for bcp, boost=myboost
        "multithreading": [True, False],  # enables multithreading support
        "numa": [True, False],
        "zlib": [True, False],
        "bzip2": [True, False],
        "lzma": [True, False],
        "zstd": [True, False],
        "segmented_stacks": [True, False],
        "debug_level": list(range(0, 14)),
        "pch": [True, False],
        "extra_b2_flags": [None, "ANY"],  # custom b2 flags
        "i18n_backend": ["iconv", "icu", None, "deprecated"],
        "i18n_backend_iconv": ["libc", "libiconv", "off"],
        "i18n_backend_icu": [True, False],
        "visibility": ["global", "protected", "hidden"],
        "addr2line_location": ["ANY"],
        "with_stacktrace_backtrace": [True, False],
        "buildid": [None, "ANY"],
        "python_buildid": [None, "ANY"],
        "system_use_utf8": [True, False],
        "without_python": [True, False],
        "without_mpi": [True, False],
        "without_graph_parallel": [True, False],
        "without_context": [True, False],
        "without_contract": [True, False],
        "without_coroutine": [True, False],
        "without_fiber": [True, False],
        "without_graph": [True, False],
        "without_graph_parallel": [True, False],
        "without_iostreams": [True, False],
        "without_locale": [True, False],
        "without_log": [True, False],
        "without_math": [True, False],
        "without_mpi": [True, False],
        "without_nowide": [True, False],
        "without_program_options": [True, False],
        "without_python": [True, False],
        "without_random": [True, False],
        "without_serialization": [True, False],
        "without_stacktrace": [True, False],
        "without_test": [True, False],
        "without_timer": [True, False],
        "without_type_erasure": [True, False],
        "without_wave": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "header_only": False,
        "error_code_header_only": False,
        "system_no_deprecated": False,
        "asio_no_deprecated": False,
        "filesystem_no_deprecated": False,
        "filesystem_use_std_fs": False,
        "filesystem_version": None,
        "layout": "system",
        "magic_autolink": False,
        "diagnostic_definitions": False,
        "python_executable": None,
        "python_version": None,
        "namespace": "boost",
        "namespace_alias": False,
        "multithreading": True,
        "numa": True,
        "zlib": True,
        "bzip2": True,
        "lzma": False,
        "zstd": False,
        "segmented_stacks": False,
        "debug_level": 0,
        "pch": True,
        "extra_b2_flags": None,
        "i18n_backend": "deprecated",
        "i18n_backend_iconv": "libc",
        "i18n_backend_icu": False,
        "visibility": "hidden",
        "addr2line_location": "/usr/bin/addr2line",
        "with_stacktrace_backtrace": True,
        "buildid": None,
        "python_buildid": None,
        "system_use_utf8": False,
        "without_python":True,
        "without_mpi":True,
        "without_graph_parallel":True,
        "without_context":True,
        "without_contract":True,
        "without_coroutine":True,
        "without_fiber":True,
        "without_graph":True,
        "without_graph_parallel":True,
        "without_iostreams":True,
        "without_locale":True,
        "without_log":True,
        "without_math":True,
        "without_mpi":True,
        "without_nowide":True,
        "without_program_options":True,
        "without_python":True,
        "without_random":True,
        "without_serialization":True,
        "without_stacktrace":True,
        "without_test":True,
        "without_timer":True,
        "without_type_erasure":True,
        "without_wave":True,
    }

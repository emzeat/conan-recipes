# conanfile.py
#
# Copyright (c) 2022 - 2023 Marius Zwicker
# All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from conans import ConanFile, tools, CMake
from conan.tools.cmake import CMakeToolchain, cmake_layout
import os

class QtKeychainConan(ConanFile):
    name = "qtkeychain"
    description = "Platform-independent Qt API for storing passwords securely."
    url = "https://emzeat.de/conan-recipes/recipes/qtkeychain"
    homepage = "https://github.com/frankosterfeld/qtkeychain"
    license = "Modified BSD License"
    version = "0.9.1"

    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "translations": [True, False],
        "with_libsecret": [True, False],
        "with_credentialstore": [True, False]
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "translations": False,
        "with_libsecret": True,
        "with_credentialstore": True
    }

    short_paths = True
    generators = "cmake", "cmake_find_package"

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    @property
    def _build_subfolder(self):
        return "build_subfolder"

    def export_sources(self):
        for patch in self.conan_data.get("patches", {}).get(self.version, []):
            self.copy(patch["patch_file"])

    def source(self):
        tools.get(
            **self.conan_data["sources"][self.version],
            strip_root=True, destination=self._source_subfolder)
        for patch in self.conan_data.get("patches", {}).get(self.version, []):
            print(f"Patch source: {patch}")
            tools.patch(**patch)

    def build_requirements(self):
        if self.settings.os == "Windows" and self.settings.compiler == "Visual Studio":
            self.build_requires("jom/1.1.3")

    def requirements(self):
        self.requires("qt/5.15.4")
        if self.options.with_libsecret:
            self.requires("libsecret/0.20.5")

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC
        if self.settings.os != "Linux":
            self.options.with_libsecret = False

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_MODULE_PATH"] = self.build_folder.replace("\\", "/")
        cmake.definitions["BUILD_WITH_QT4"] = "OFF"
        cmake.definitions["BUILD_TEST_APPLICATION"] = "OFF"
        cmake.definitions["BUILD_TRANSLATIONS"] = self.options.translations
        cmake.definitions["QTKEYCHAIN_STATIC"] = not self.options.shared
        cmake.definitions["LIBSECRET_SUPPORT"] = self.options.with_libsecret
        cmake.definitions["USE_CREDENTIAL_STORE"] = self.options.with_credentialstore
        cmake.definitions["CMAKE_CXX_STANDARD"] = 11

        cmake.configure(source_folder=self._source_subfolder,
                        build_folder=self._build_subfolder)
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()
        self.copy("COPYING", src=self._source_subfolder, dst="licenses")

    def package_info(self):
        self.cpp_info.set_property("cmake_file_name", "Qt5Keychain")
        self.cpp_info.set_property("cmake_target_name", "Qt5Keychain::Qt5Keychain")
        self.cpp_info.libs = ["qt5keychain"]
        self.cpp_info.requires = ["qt::qtCore"]
        if self.settings.os == "Linux":
            self.cpp_info.requires.append("qt::qtDBus")
        if self.options.with_libsecret:
            self.cpp_info.requires.append("libsecret::libsecret")
        elif self.settings.os in ["Macos", "iOS"]:
            self.cpp_info.frameworks.append("CoreFoundation")
            self.cpp_info.frameworks.append("Security")
        elif self.settings.os == "Windows" and not self.options.with_credentialstore:
            self.cpp_info.system_libs.append("crypt32")

        self.cpp_info.names["cmake_find_package"] = "Qt5Keychain"
        self.cpp_info.names["cmake_find_package_multi"] = "Qt5Keychain"

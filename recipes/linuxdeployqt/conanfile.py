# conanfile.py
#
# Copyright (c) 2022 - 2023 Marius Zwicker
# All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from conans import ConanFile, tools
from conan.tools import files
import glob, os
from pathlib import Path

class LinuxDeployQt(ConanFile):
    name = "linuxdeployqt"
    description = "Makes Linux applications self-contained."
    url = "https://emzeat.de/conan-recipes/recipes/linuxdeployqt"
    homepage = "https://github.com/probonopd/linuxdeployqt"
    license = "GPLv3"
    version = "8.0"

    settings = "os"

    @property
    def _os_build(self):
        return str(self.settings.os)

    @property
    def _binary_data(self):
        return self.conan_data["binaries"][self.version][self._os_build]

    def build(self):
        # data is platform specific so we need to fetch it
        # as part of build() and not source()
        tools.download(**self._binary_data, filename='linuxdeployqt.AppImage')
        with tools.chdir(self.build_folder):
            for f in glob.glob("*"):
                os.chmod(f, 0o755)
        self.run(f'./linuxdeployqt.AppImage --appimage-extract')

    def package(self):
        for subdir in ['bin', 'lib', 'share', 'translations']:
            self.copy("*", src=f"squashfs-root/usr/{subdir}", dst=subdir)
        with tools.chdir(os.path.join(self.package_folder, "bin")):
            for f in glob.glob("*"):
                os.chmod(f, 0o755)
        tools.save(os.path.join(self.package_folder, "lib", "cmake", "linuxdeployqt-module.cmake"), f"""
            if(NOT TARGET linuxdeployqt::linuxdeployqt)
                add_executable(linuxdeployqt::linuxdeployqt IMPORTED GLOBAL)
            endif()
            set_property(TARGET linuxdeployqt::linuxdeployqt PROPERTY IMPORTED_LOCATION ${{linuxdeployqt_RES_DIRS}}/linuxdeployqt)
            set( LINUX_DEPLOY_QT ${{linuxdeployqt_RES_DIRS}}/linuxdeployqt CACHE PATH "linuxdeployqt image" FORCE )

            if(NOT TARGET linuxdeployqt::appimagetool)
                add_executable(linuxdeployqt::appimagetool IMPORTED GLOBAL)
            endif()
            set_property(TARGET linuxdeployqt::appimagetool PROPERTY IMPORTED_LOCATION ${{linuxdeployqt_RES_DIRS}}/appimagetool)

            if(NOT TARGET linuxdeployqt::desktop-file-validate)
                add_executable(linuxdeployqt::desktop-file-validate IMPORTED GLOBAL)
            endif()
            set_property(TARGET linuxdeployqt::desktop-file-validate PROPERTY IMPORTED_LOCATION ${{linuxdeployqt_RES_DIRS}}/desktop-file-validate)
        """)

    def package_info(self):
        self.cpp_info.set_property("cmake_file_name", "linuxdeployqt")
        self.cpp_info.build_modules["cmake_find_package"] = ["lib/cmake/linuxdeployqt-module.cmake"]
        self.cpp_info.set_property("cmake_build_modules", ["lib/cmake/linuxdeployqt-module.cmake"])

        self.cpp_info.bindirs = ['bin']
        self.cpp_info.resdirs = ['bin']
        self.cpp_info.includedirs = []
        self.cpp_info.libdirs = []
        self.cpp_info.libs = []

        self.cpp_info.names["cmake_find_package"] = "linuxdeployqt"
        self.cpp_info.names["cmake_find_package_multi"] = "linuxdeployqt"

        bin_path = os.path.join(self.package_folder, "bin")
        self.output.info(f"Appending PATH environment variable: {bin_path}")
        self.env_info.path.append(bin_path)

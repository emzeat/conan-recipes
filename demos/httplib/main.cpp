/*
 * main.cpp
 *
 * Copyright (c) 2022 - 2023 Marius Zwicker
 * All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define CPPHTTPLIB_OPENSSL_SUPPORT
#include <httplib.h>

int
main(int argc, char* argv[])
{
    static_cast<void>(argc);
    static_cast<void>(argv);

    // HTTPS
    httplib::SSLServer svr("./cert.pem", "./key.pem");

    svr.Get("/hi", [](const httplib::Request &, httplib::Response &res) {
        res.set_content("Hello World!", "text/plain");
    });

    svr.listen("0.0.0.0", 8080);

    // HTTPS
    httplib::Client cli("https://cpp-httplib-server.yhirose.repl.co");
    httplib::SSLClient sslCli("https://cpp-httplib-server.yhirose.repl.co");

    // Use your CA bundle
    sslCli.set_ca_cert_path("./ca-bundle.crt");

    // Disable cert verification
    sslCli.enable_server_certificate_verification(false);

    auto res = cli.Get("/hi");
    static_cast<void>(res->status);
    static_cast<void>(res->body);

    return 0;
}

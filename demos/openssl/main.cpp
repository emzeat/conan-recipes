/*
 * main.cpp
 *
 * Copyright (c) 2022 - 2023 Marius Zwicker
 * All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <openssl/ssl.h>
#include <openssl/crypto.h>

int
main(int argc, char* argv[])
{
    static_cast<void>(argc);
    static_cast<void>(argv);

    // ensure ssl can be used
    static_cast<void>(SSL_CIPHER_get_version(nullptr));

    // ensure crypto can be used
    static_cast<void>(CRYPTO_secure_used());

    return 0;
}

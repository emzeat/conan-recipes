/*
 * main.cpp
 *
 * Copyright (c) 2022 - 2023 Marius Zwicker
 * All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <avahi-client/client.h>
#include <avahi-common/simple-watch.h>
#include <dns_sd.h>

static void
client_callback(AvahiClient* /* c */,
                AvahiClientState /* state */,
                void* /* userdata */)
{}

int
main(int argc, char* argv[])
{
    static_cast<void>(argc);
    static_cast<void>(argv);

    // avahi-client
    AvahiSimplePoll* simple_poll = avahi_simple_poll_new();
    if (simple_poll) {
        int error = 0;
        AvahiClient* client =
          avahi_client_new(avahi_simple_poll_get(simple_poll),
                           static_cast<AvahiClientFlags>(0),
                           client_callback,
                           nullptr,
                           &error);
        if (client) {
            avahi_client_free(client);
        }
        avahi_simple_poll_free(simple_poll);
    }

    // dns-sd compatibility
    DNSServiceRef sdRef = nullptr;
    DNSServiceBrowse(&sdRef, 0, 0, "_example._tcp", nullptr, nullptr, nullptr);

    return 0;
}

# Conan Recipes

This is a repository used for vetting conan recipes for
use across the different projects and platforms supported.

Recipes get pulled directly from conan.io/center whenever
possible but some of them have been forked when issues
have been found and upstreaming of fixes for these is
still ongoing.

## Using these recipes

In order to use these recipes just add our Codeberg conan
mirror to your list of conan remotes:

```bash
conan remote add emzeat https://codeberg.org/api/packages/emzeat/conan
```

Head to https://codeberg.org/emzeat/-/packages/conan for browsing
the list of recipes tracked in addition to conan-center.

Please resort to the conan documentation at https://docs.conan.io
for further detail.

# conanfile.py
#
# Copyright (c) 2023 - 2024 Marius Zwicker
# All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain, CMakeDeps
from conan.tools.files import copy
import os
from pathlib import Path

class RecipesConan(ConanFile):
    name='conan-playground'
    description='Project to test various recipes consumed in other places'
    homepage='https://codeberg.org/emzeat/conan-recipes'
    url='https://codeberg.org/emzeat/conan-recipes'
    license='Apache-2.0'
    package_type="application"
    settings = "os", "compiler", "build_type", "arch"

    def requirements(self):
        self.requires("crowcpp-crow/1.0+5#d9178d61379608c57aba63faf5f12d76")
        self.requires("exiv2/0.27.7@emzeat/external")
        self.requires("glog/0.6.0#cb2e87e1b3c9567cc6a8a25de17ddd4e")
        self.requires("gtest/1.14.0#4372c5aed2b4018ed9f9da3e218d18b3")
        self.requires("gflags/2.2.2#48d1262ffac8d30c3224befb8275a533")
        self.requires("libgit2/1.3.0@emzeat/external")
        self.requires("openimageio/2.2.20.0@emzeat/external")
        self.requires("xz_utils/5.4.4#f7cff24c0eadb6e51001dd86695e823d", override=True)
        self.requires("lcms/2.14@emzeat/external")
        self.requires("qt/5.15.11@emzeat/external")
        self.requires("sqlite3/3.43.1#6e8f928a7e54fecc11e2af94e1ca8bcd")
        self.requires("zlib/1.2.13#97d5730b529b4224045fe7090592d4c1")
        self.requires("minizip/1.2.12#6b08dc7b8f37c1149c2f1cdc301f1b0d")
        if self.settings.os == "Macos" and self.settings.arch == "armv8":
            self.requires("boost/1.83.0+universal@emzeat/external")
            self.requires("openssl/1.1.1w+universal@emzeat/external")
            self.requires("libjpeg-turbo/2.1.2+universal@emzeat/external", override=True)
            self.requires("libpng/1.6.40+universal@emzeat/external", override=True)
        else:
            self.requires("boost/1.83.0#e948ecbe69e6e240658ba2eda46ba8e8")
            self.requires("openssl/1.1.1w#48acccc156e0cd9d775a2ebb4f0aa0ba")
            self.requires("libpng/1.6.40", override=True)
        #self.requires("capnproto/1.0.1")
        self.requires("protobuf/3.21.12#ff413e7c6d81724e872661875af83e68")
        self.requires("cpp-httplib/0.11.1#9f5711e479cbec98f784602f8553baa3")
        self.requires("perfetto/27.1#5da7234912f6badf74350c4e4e045d16")
        self.requires("mdns/1.4.2#9d2e40713a4f294223ac08576f964dc2")
        if self.settings.os == "Linux":
            self.requires("avahi/0.8@emzeat/external")
            self.requires("expat/2.4.8", override=True)
            self.requires("libiconv/1.17", override=True)
            self.requires("pcre2/10.39", override=True)
            self.requires("glib/2.78.1", override=True)
            self.requires("libffi/3.4.3", override=True)
            self.requires("dbus/1.12.20", override=True)
            self.requires("libdispatch/5.9.2@emzeat/external")
        self.requires("qtkeychain/0.9.1@emzeat/external")
        self.requires("qzxing/3.3.0@emzeat/external")

    def build_requirements(self):
        self.tool_requires("clang-tools-extra/13.0.1@emzeat/external")
        #self.tool_requires("clang-tools-extra/15.0.7@emzeat/external")
        #self.tool_requires("capnproto/1.0.1")
        self.tool_requires("protobuf/3.21.12#ff413e7c6d81724e872661875af83e68")
        self.tool_requires("ccache/4.8.3#af66e078d92802aed585b4a9428d7e35")
        if self.settings.os == "Linux":
            self.tool_requires("linuxdeployqt/8.0@emzeat/external")

    def generate(self):
        for dep in self.dependencies.values():
            if dep.package_folder:
                copy(self, "license*", dep.package_folder, self.build_path / '3rdparty' / dep.ref.name, ignore_case=True)

        deps = CMakeDeps(self)
        deps.build_context_activated = ["clang-tools-extra", "linuxdeployqt"]
        deps.build_context_build_modules = ["clang-tools-extra", "linuxdeployqt"]
        deps.generate()

        tc = CMakeToolchain(self)
        tc.user_presets_path = None
        tc.generate()

        # fixup detection of cross tools
        protoc = Path(self.dependencies.direct_build['protobuf'].package_folder) / ("bin/protoc.exe" if os.name == 'nt' else "bin/protoc")
        protobuf_cmake = self.generators_path / 'protobuf-config.cmake'
        protobuf_cmake_text = protobuf_cmake.read_text()
        if protoc.as_posix() not in protobuf_cmake_text:
            protobuf_cmake.write_text(f'set(PROTOC_PROGRAM "{protoc.as_posix()}")\n\n' + protobuf_cmake_text)

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

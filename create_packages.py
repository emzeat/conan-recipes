#!/usr/bin/env python3
# create_packages.py
#
# Copyright (c) 2022 - 2024 Marius Zwicker
# All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
from subprocess import check_call
from conan import conan_version

REMOTE = 'emzeat'

# packages used from upstream but with patches
FORKED_PACKAGES = [
    ('openimageio/2.2.20.0@emzeat/external', 'recipes/openimageio'),
    ('qt/5.15.11@emzeat/external', 'recipes/qt'),
    #('capnproto/0.10.1@emzeat/external', 'recipes/conan-center-index/recipes/capnproto/all'),
    ('avahi/0.8@emzeat/external', 'recipes/avahi'),
    ('dbus/1.12.20@emzeat/external', 'recipes/dbus'),
    ('meson/1.3.0-rc1@emzeat/external', 'recipes/meson'),
    ('libgit2/1.3.0@emzeat/external', 'recipes/libgit2'),
    ('lcms/2.14@emzeat/external', 'recipes/lcms'),
    ('exiv2/0.27.7@emzeat/external', 'recipes/exiv2'),
]

# custom packages
CUSTOM_PACKAGES = [
    ('qtkeychain/0.9.1@emzeat/external', 'recipes/qtkeychain'),
    ('qzxing/3.3.0@emzeat/external', 'recipes/qzxing'),
    ('clang-tools-extra/13.0.1@emzeat/external', 'recipes/clang-tools-extra'),
    ('clang-tools-extra/15.0.7@emzeat/external', 'recipes/clang-tools-extra'),
    ('linuxdeployqt/8.0@emzeat/external', 'recipes/linuxdeployqt'),
]
if sys.platform == 'darwin':
    CUSTOM_PACKAGES += [
        ('universal_binary/0.1.0@emzeat/external', 'recipes/universal/base'),
        ('openssl/1.1.1w+universal@emzeat/external', 'recipes/universal/openssl'),
        ('libjpeg-turbo/2.1.2+universal@emzeat/external', 'recipes/universal/libjpeg-turbo'),
        ('libpng/1.6.40+universal@emzeat/external', 'recipes/universal/libpng'),
        ('boost/1.83.0+universal@emzeat/external', 'recipes/universal/boost'),
    ]
if sys.platform == 'linux':
    CUSTOM_PACKAGES += [
        ('libdispatch/5.9.2@emzeat/external', 'recipes/libdispatch'),
    ]


for package, recipe in FORKED_PACKAGES + CUSTOM_PACKAGES:
    sys.stdout.write(f"-- conan: Exporting: {package}\n")
    sys.stdout.flush()
    if conan_version.major < 2:
        check_call(['conan', 'export', recipe, package])
    else:
        name_version, user_channel = package.split('@')
        name, version = name_version.split('/')
        user, channel = user_channel.split('/')
        check_call(['conan', 'export', '--name', name, '--user', user, '--channel', channel, '--version', version, recipe])
    #check_call(['conan', 'upload', '-r', REMOTE, '--confirm', '--check', package],
    #           encoding='utf8', cwd=recipe)
